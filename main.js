require('waitjs');

var cheerio = require('cheerio'),
    request = require('request'),
    fs = require('fs'),
    open = require('open');

var url = 'http://tnttorrent.info/tags.php?tags=1%2C58%2C95&search=vikings+s03&active=0&minsize=&maxsize=&acc=v2';
var counter = 1;

function getUrl()
{
	// test success case
	// if (counter > 1) {
	// 	return 'http://tnttorrent.info/tags.php?where=0&search=vikings&acc=v2';
	// }

	return url;
}

var check = function () {
	request(getUrl(), function (err, response, body) {
	    $ = cheerio.load(body);

	    if (err) {
	    	console.log(err);
	    }

	    var noResults = $('table.centerblock_content p.error');
	    if (noResults.text() === 'Nie ma torrentów...') {
	        process.stdout.write("Try: "+ counter++ +" - jeszcze nie ma :(\033[0G");
	    } else {
	        var results = $('table.lista > tr > td > table.styled-block td.lista');
	        if (results.length > 0) {
	            console.log(getUrl());
	            fs.readFile('boat.txt', function (err, data) {
	                if (err) {
	                    throw err;
	                }

	                console.log(data.toString());
	                // clear all setIntervals that have id set
	                clear();
	                // open default browser
	                open(getUrl());
	            });
	        }
	    }
	});
}

if (process.argv[2] && process.argv[2] === 'loop') {
	repeat('5 min', function () {check()}, 'lookingForRagnar');
} else {
	check();
}
